clear
echo --------------------
echo Fermeture des dockers persistents
echo --------------------
docker ps
docker stop frontend nodeapp redis-srv
docker rm frontend nodeapp redis-srv
docker ps
echo --------------------
echo Validez en utilisant internet explorer
echo --------------------
echo "iexplore http://`curl ifconfig.co`:80"
echo --------------------
echo Build from docker-compose-sg repository
echo --------------------
git pull
echo --------------------
echo Demarrage du docker-compose
echo --------------------
docker-compose up

